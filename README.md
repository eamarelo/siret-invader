# Siret-Invdaer

Here you can find the documentation of the siret-invader project.


# Prerequisite


## Create files and folders

Before run the project you have to put the csv files inside `models/`.
The name  of the file need to be `StockEtablissement_utf8.csv`

You need too to create folder in `models` folder intitulate `outputs`
You can also run this cli inside project's folder : `mkdir models/outputs`

## Node.js & NPM

You need to install node.js and npm

## Global dependencies

You need to install pm2 `npm install pm2 -g`

# Run project

To run this project : `npm i && npm start`

## eslint

when you run npm start you run too eslint 

## Save Mode

After have to split files in 113 chunks , the program creates a files `succesSplit.json`.
When you re-run the apps you don't need to split the CSV files.
You go direcly to the startChildProcess function.

## Start where you finish

The process deletes files after bulk to mongodb, this function permites to start where you stop the app's process.

`If you want to re-run the apps after all process finished, you have to delete files successSplit.json` or you can run this cli inside project folder :
`rm -rf succesSplit.json`
