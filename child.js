const Lazy = require('lazy')
const fs = require('fs')
const mongoose = require('mongoose')
const modelField = require('./models/modelData')
const siretModel = require('./models/siretModel')

const handledSize = 10000

const uri = 'mongodb://localhost:27017/siret'
mongoose.connect(uri, { useNewUrlParser: true })

const errorHandler = (err) => {
  if (!err) {
    console.log(err)
  }
  return err
}

process.send({
  type: 'process:msg',
  data: {
    success: true
  }
})
/**
  * process.on.
  * @param {packet} json.
*/
process.on('message', (packet) => {
  const fileSent = packet.data.file

  /**
  * streamFile.
  * @param {omitLine} int.
  * @param {handle} int.
*/
  const streamFile = (omitLine = 0, handle = 0) => (
    new Promise(async (resolve, reject) => {
      try {
        const fileStreamed = await fs.createReadStream(fileSent)
        await (new Lazy(fileStreamed))
          .lines
          .skip(omitLine)
          .take(handle)
          .map(data => data.toString().split(','))
          .join(result => resolve({
            result,
            omitLine,
            handle
          }))
      } catch (err) {
        reject(err)
      }
    })
  )
  /**
  * streamToMongo.
  * @param {omitLine} int.
  * @param {handle} int.
*/
  const streamToMongo = (omitLine = 0, handle = 0, callback) => {
    streamFile(omitLine, handle).then(({ result }) => {
      if (result.length === 0) {
        console.log('NEXT FILE')
        callback(true)
      } else {
        siretModel.bulkWrite(result.map(i => ({
          insertOne: {
            document: modelField(i)
          }
        }))).then(() => {
          console.log('NEXT 10 000 LINES')
          streamToMongo(1000000, handle, callback)
          fs.unlink(fileSent, errorHandler)
        }).catch(e => console.error(e))
      }
    }).catch(error => console.error(error))
  }
  mongoose.connection.on('open', (err) => {
    if (err) {
      process.exit(1)
    } else {
      streamToMongo(0, handledSize, () => {
        process.exit(1)
      })
    }
  })
})
