const fs = require('fs')
const csvSplitStream = require('csv-split-stream')
const pm2 = require('pm2')

const dest = './models/outputs/'

let promise = null

/**
  * errorHandler.
  * @param {Error} err.
*/
const errorHandler = (err) => {
  if (!err) {
    console.log(err)
  }
  return err
}

/**
  * splitCsv.
  * @param {Path} file.
  * return {Boolean}
*/
const splitCsv = () => (
  new Promise((resolve, reject) => {
    resolve(
      csvSplitStream.split(
        fs.createReadStream('./models/StockEtablissement_utf8.csv'),
        {
          lineLimit: 250000
        },
        index => fs.createWriteStream(`${dest}output-${index}.csv`)
      )
        .then((csvSplitResponse) => {
          fs.writeFile('successSplit.json', csvSplitResponse.totalChunks, errorHandler)
        }).catch((csvSplitError) => {
          console.log('csvSplitStream failed!', csvSplitError)
        })
    )
    reject(console.log('reject'))
  })
)

/**
  * startChildProcess.
  * @param {pm2} function.
*/
const startChildProcess = () => {
  const dir = fs.readdirSync('./models/outputs')
  pm2.connect((err) => {
    if (err) {
      console.error(err)
      process.exit(2)
    }
    pm2.start('./ecosystem.config.js', (errStart, apps) => {
      if (!apps) {
        console.log(errStart)
      }
      pm2.launchBus((errBus, bus) => {
        if (errBus) {
          console.log(errBus)
        }
        bus.on('process:msg', (packet) => {
          if (dir.length === 0) {
            console.log('Process is finish ! Go kill the childs')
            pm2.delete(packet.process.pm_id)
            pm2.delete(0)
          } else {
            console.log('Le process is loading')
            pm2.sendDataToProcessId({
              type: 'process:msg',
              data: {
                file: dest + dir[0]
              },
              id: packet.process.pm_id,
              topic: 'SEND FILES'
            }, (errSend, res) => {
              if (!res) {
                console.log(errSend)
              }
              dir.shift()
            })
          }
        })
      })
      if (err) throw err
    })
  })
}

/**
  * Conditions check if files exist.
  * @param {promise} file.
  * return {Boolean}
*/
if (fs.existsSync('successSplit.json')) {
  console.log('CSV files is already split in chunk of 50Mo.')
  promise = Promise.resolve()
} else {
  console.log('CSV files is spliting')
  promise = splitCsv()
}

promise.then(() => {
  startChildProcess()
}).catch((err) => {
  console.log('catching err', err)
})
