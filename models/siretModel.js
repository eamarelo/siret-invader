const mongoose = require('mongoose')

const siretModel = new mongoose.Schema({}, {
  strict: false,
  collection: 'enterprises'
})
mongoose.model('enterprises', siretModel)

module.exports = mongoose.model('enterprises');
